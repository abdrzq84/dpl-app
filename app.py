from __future__ import print_function
import tensorflow as tf;
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
import pandas as pd
import sys
from flask import Flask, redirect, url_for, render_template, request, jsonify

app = Flask(__name__)

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session =tf.compat.v1.InteractiveSession(config=config)

# def eprint(*args, **kwargs):
#     print(*args, file=sys.stderr, **kwargs)

df = pd.read_csv("dataset.csv", sep=";")
text = df["Tweet"].tolist()
token = Tokenizer()
token.fit_on_texts(text)
model = tf.keras.models.load_model('./textcls.tf')
# eprint('Model Ready')

def model_predic(input_text):
    max_kata = 50
    text = token.texts_to_sequences(input_text)
    text = pad_sequences(text,maxlen=max_kata,padding="post")
    preds = model.predict(text)[0]
    preds_class = np.argmax(preds)
    acc = "{:.1%}".format(preds[preds_class])
    # print(preds_class, acc)
    classes = ['Neutral','Positive', 'Negative']
    return classes[preds_class],acc

# app.logger.info('testing info log')

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        text_content = request.form['content']
        list_text = []
        list_text.append(text_content)
        hasil = str(model_predic(list_text))
        return render_template('index.html',hasil=hasil)
    else: 
        return render_template('index.html')
    
@app.route('/api', methods=['POST'])
def get_text():
    # text_content = text
    text_content = request.form['text']
    list_text = []
    list_text.append(text_content)
    # hasil = str(model_predic(list_text))
    hasil = model_predic(list_text)
    return jsonify({'hasil': hasil[0],'akurasi': hasil[1]})

# @app.route('/predict', methods=['GET', 'POST'])
# def button_clicked():
#     if request.method == 'POST':
#         f = request
#     # print('Hello world!', file=sys.stderr)
#     # return redirect(url_for('/home'))

#     return None

if __name__ == "__main__":
    app.run(debug =True)